package com.bigbank.mugloar;

public class Dragon {
	
	private Integer scaleThickness;
	private Integer clawSharpness;
	private Integer wingStrength;
	private Integer fireBreath;
	
	public Dragon(Integer i, Integer j, Integer k, Integer l) {
		this.scaleThickness = i;
		this.clawSharpness = j;
		this.wingStrength = k;
		this.fireBreath = l;
	}
	public Integer getScaleThickness() {
		return scaleThickness;
	}
	public void setScaleThickness(Integer scaleThickness) {
		this.scaleThickness = scaleThickness;
	}
	public Integer getClawSharpness() {
		return clawSharpness;
	}
	public void setClawSharpness(Integer clawSharpness) {
		this.clawSharpness = clawSharpness;
	}
	public Integer getWingStrength() {
		return wingStrength;
	}
	public void setWingStrength(Integer wingStrength) {
		this.wingStrength = wingStrength;
	}
	public Integer getFireBreath() {
		return fireBreath;
	}
	public void setFireBreath(Integer fireBreath) {
		this.fireBreath = fireBreath;
	}

}
